import java.util.Scanner;

public class Converter {
    public static void main(String[] args) {
        System.out.println("1 - Miles to kilometers");
        System.out.println("2 - Kilometers to miles");
        System.out.println("3 - Gallons to liters");
        System.out.println("4 - Liters to gallons");
        System.out.println("5 - Yards to meters");
        System.out.println("6 - Meters to yards");
        System.out.println("7 - Feet to centimeters");
        System.out.println("8 - Centimeters to feet");
        System.out.println("9 - Cal to centimeters");
        System.out.println("10 - Centimeters to cal");

        System.out.println("Choose what you wanna convert:");
        Scanner input = new Scanner(System.in);
        String choice = input.nextLine();
        System.out.println(choice);

        if (choice.matches("1|2|3|4|5|6|7|8|9|10")) {
            int number = Integer.parseInt(choice);
            System.out.println("Write amount which you wanna count:");
            String toConv = input.nextLine();
            Double inp = Double.valueOf(String.valueOf(toConv));
            double case1 = inp * 1.609344;
            double case2 = inp * 0.62137119;
            double case3 = inp * 3.78541178;
            double case4 = inp * 0.264172052;
            double case5 = inp * 0.9144;
            double case6 = inp * 1.0936133;
            double case7 = inp * 30.48;
            double case8 = inp * 0.032808399;
            double case9 = inp * 2.54;
            double case10 = inp * 0.393700787;

            switch (number) {
                case 1:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" miles there is %.2f", case1);
                    System.out.printf(" km");
                    break;
                case 2:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" km there is %.2f", case2);
                    System.out.printf(" miles");
                    break;
                case 3:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" gallons there is %.2f", case3);
                    System.out.printf(" liters");
                    break;
                case 4:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" liters there is %.2f", case4);
                    System.out.printf(" gal");
                    break;
                case 5:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" yards there is %.2f", case5);
                    System.out.printf(" meters");
                    break;
                case 6:
                    System.out.printf("%.2f", inp);
                    System.out.printf("m there is %.2f", case6);
                    System.out.printf(" yards");
                    break;
                case 7:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" ft there is %.2f", case7);
                    System.out.printf(" cm");
                    break;
                case 8:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" cm there is %.2f", case8);
                    System.out.printf(" ft");
                    break;
                case 9:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" cal there is %.2f", case9);
                    System.out.printf(" cm");
                    break;
                case 10:
                    System.out.printf("%.2f", inp);
                    System.out.printf(" cm there is %.2f", case10);
                    System.out.printf(" cal");
                    break;
                default:
            }
        } else {
            System.out.println("Write correct number");
        }
    }
}